<!DOCTYPE html>
<html>
    <head>
        <title>Calculator</title>
        <h1>Calculate</h1><br>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="main">
            <form class="frm" method="POST" action="">
                Voltage<br><input type="text" name="voltage" required="required"/><br>Voltage(V)<br><br>
                Current<br><input type="text" name="current" required="required"/><br>Ampere(A)<br><br>
                Current Rate<br><input type="text" name="currentrate" required="required"/><br>sen/kWh<br><br>
                <button class="btn" name="calculate">Calculate</button>
            </form><br><br>
            <div class="tab">
                <?php

                    function calPower($voltage, $current){
                        return $voltage * $current;
                    }

                    function calEnergy($power){
                        return $power *1000;
                    }

                    function calTotal($energy, $currentrate){
                        return $energy * ($currentrate/100);
                    }
                    
                    function rt($currentrate){
                        return $currentrate/100;
                    }

                    if ($_SERVER['REQUEST_METHOD']=='POST'){
                    $voltage=$_POST['voltage'];
                    $current=$_POST['current'];
                    $currentrate=$_POST['currentrate'];

                    // $power=($voltage*$current/1000);
                    // $energy=$power*1000;
                    // $total=$energy*($currentrate/100);
                    // $rate=$currentrate/100;

                    $power = calPower($voltage,$current)/1000;
                    $energy = calEnergy($power);
                    $total = calTotal($energy,$currentrate);
                    $rate = rt($currentrate);
                ?>
                <div class="container">
                    <p>POWER: &nbsp;&nbsp; <?php echo $power?>kw</p>
                    <p>RATE:&nbsp;&nbsp; <?php echo $rate?>RM</p>
                </div><br>
                <table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Hour</th>
                            <th>Energy(kWh)</th>
                            <th>TOTAL(RM)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $count=1;

                            for($count=1;$count>0 && $count<25; $count++){
                                $power=($voltage*$current/1000)*$count;
                                $energy=$power*$count*1000;
                                $total=$power*$rate;
                                // $rate=$currentrate/100;
                                // $total=$power*$rate;
                                echo "<tr
                                    style='border: 1px solid white;
                                    border-collapse: collapse;
                                    width: 25%;
                                    height: 25%;'
                                    text-align: center;
                                    padding: 15px;
                                    
                                >";

                                echo "<td
                                style='border: 1px solid white;
                                border-collapse: collapse;
                                width: 25%;
                                height: 25%;
                                text-align: center;
                                border-bottom: 1px solid #ccc;'
                                padding: 15px;
                                >"
                                . $count .
                                "</td>";

                                echo "<td 
                                style='border: 1px solid white;
                                border-collapse: collapse;
                                width: 25%;
                                height: 25%; 
                                text-align: center;
                                border-bottom: 1px solid #ccc;
                                padding: 15px;
                                '>"
                                . $count .
                                "</td>";

                                echo "<td style='border: 1px solid white;
                                border-collapse: collapse;
                                width: 25%;
                                height: 25%;
                                text-align: center;
                                border-bottom: 1px solid #ccc;
                                padding: 15px;
                                '>"
                                . $power .
                                "</td>";

                                echo "<td 
                                style='border: 1px solid white;
                                border-collapse: collapse;
                                width: 25%;
                                height: 25%;
                                text-align: center;
                                border-bottom: 1px solid #ccc;
                                padding: 15px;
                                '>"
                                . round($total, 2) .
                                "</td>";
                                echo "</tr>";
                                
                            };}
                        ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
